import Signals from "../../competitor/signals/signals";
import PriceHistory from "../../competitor/priceHistory/priceHistory";
const SingleCompetitorTabs = ({data,activeKey}) => {
    
    return(
        <div className="competitor-tab">
            {activeKey === 'signals' && <Signals data={data}/> }
            {activeKey === 'price-history' && <PriceHistory data={data}/> }
        </div>
    )
}
export default SingleCompetitorTabs;