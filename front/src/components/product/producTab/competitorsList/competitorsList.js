import { useState } from 'react';
import Sorting from "@/components/global/sorting";
import SingleCompetitorList from './singleCompetitorList';
const CompetitorsList = ({data}) => {

    const [searchResults, setSearchResults] = useState(data);
    const [activeMenuId, setActiveMenuId] = useState(null);  
    const [activeSingleCompetitor, setActiveSingleCompetitor] = useState(null);

    const handlerIdSingle = (id) =>{
        setActiveSingleCompetitor(id)
    }
    const sortingNames = [
        {name: "Competitor Name"},
        {name: "Competitor Status"}
    ]
  
    const sortingHandler = (sortKey) => {
        const sortedResults = [...searchResults].sort((a, b) => { 
            const valueA = a[sortKey]?.toLowerCase() || '';
            const valueB = b[sortKey]?.toLowerCase() || '';
    
            if (valueA < valueB) return -1;
            if (valueA > valueB) return 1;
            return 0;
        }); 
        setSearchResults(sortedResults); 
    };
    
    const selectionHandler = (value) => {
        // TODO: Make selection handler
        console.log(value)
    }
    const toggleMenu = (id) => {
        if (activeMenuId === id) {
            setActiveMenuId(null);   
        } else {
            setActiveMenuId(id);    
        }
    };
    return(
        <div className='competitors-list'>
            <div className='list'>
                <Sorting selectionCallBack={selectionHandler} callBack={sortingHandler} names={sortingNames}/>                 
                {searchResults.map(competitor => (
                    <SingleCompetitorList 
                    toggler={toggleMenu} 
                    isActive={activeMenuId === competitor.id} 
                    data={competitor} 
                    key={competitor.id}
                    handlerIdSingle={handlerIdSingle}
                    isActiveSingle= {activeSingleCompetitor === competitor.id}
                    />
                ))}
            </div>            
        </div>
    )
}
export default CompetitorsList;