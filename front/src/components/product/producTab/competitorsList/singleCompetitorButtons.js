import "@/styles/scss/components/singleCompetitorButtons.scss";
const SingleCompetitorButtons = ({ callBack, activeButton }) => {
    const btnHandler = (key) => {
        callBack(key);  // Call parent handler to update state
    }
    return (
        <div className="single-competitor-buttons">
            <ul>
                <li>
                    <button
                        className={`btn btn-badge ${activeButton === 'signals' ? 'active' : ''}`}
                        onClick={() => btnHandler('signals')}
                    >
                        Signals
                    </button>
                </li>
                <li>
                    <button
                        className={`btn btn-badge ${activeButton === 'detailed-comparison' ? 'active' : ''}`}
                        onClick={() => btnHandler('detailed-comparison')}
                    >
                        Detailed Comparison
                    </button>
                </li>
                <li>
                    <button
                        className={`btn btn-badge ${activeButton === 'price-history' ? 'active' : ''}`}
                        onClick={() => btnHandler('price-history')}
                    >
                        Price History
                    </button>
                </li>
            </ul>
        </div>
    );
};
export default SingleCompetitorButtons;
