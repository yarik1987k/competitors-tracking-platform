import { useState, useEffect } from 'react';
import SingleCompetitorButtons from './singleCompetitorButtons';
import SingleCompetitorTabs from './singleCompetitorTabs';
import SmallMenu from '@/components/global/smallMenu';
import '@/styles/scss/components/list.scss';
const SingleCompetitorList = ({ data, toggler, isActive, handlerIdSingle, isActiveSingle }) => {
    const [activeButton, setActiveButton] = useState(null);
    const menuItems = [
        { label: 'Edit', action: 'edit' },
        { label: 'Delete', action: 'delete' }, 
        { label: 'Change Status', action: 'change-status' }
    ];
    const btnHandler = (key) => {
        if (activeButton === key) {
            setActiveButton(null);  
            handlerIdSingle(null); 
        } else {
            setActiveButton(key);
            handlerIdSingle(data.id); 
        } 
    };
    const menuCallBack = (key) => {
        toggler(data.id);
        // TODO: Add server functionality
        console.log(key)
    }; 

    const menuToggler = (event) => {
        event.stopPropagation();   
        toggler(data.id);
    };  

    useEffect(()=>{
        if(isActiveSingle === false )  setActiveButton(null) ;
    },[isActiveSingle])
    
 
    return (
        <div className={`single-list ${isActiveSingle && 'active'}`}>
            <div className='single-list-container'>
                <div className='selection'>
                    <input type='checkbox' value={data.id} />
                </div>   
                <div className="d-flex justify-content-start ps-5 row row-cols-2">     
                <div className='single-col'> 
                        <div className='single-col-content'>
                            <a href={`#${data.id}`}>{data.name}</a>
                            <p>{data.competitorWebsite}</p>
                        </div> 
                </div>
                <div className='single-col status-col'>
                    <span className='list-status'><span className={`badge badge-${data.status}`}>{data.status}</span></span>
                </div>
                <div className='single-col single-func'>
                    <div className='d-flex justify-content-end column-gap-3'>
                        <SingleCompetitorButtons 
                            callBack={btnHandler} 
                            activeButton={activeButton}  
                        />
                        <div className='list-options'>
                            <button onClick={menuToggler}>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5ZM12 12.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5ZM12 18.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5Z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                    
                    {isActive && <SmallMenu items={menuItems}  onItemSelected={menuCallBack} />}
                </div>
                </div>
            </div> 
            {isActiveSingle && 
                <SingleCompetitorTabs 
                    data={data} 
                    activeKey={activeButton} 
                />            
            }
        </div>
    );
};

export default SingleCompetitorList;
