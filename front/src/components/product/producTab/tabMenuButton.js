'use client'
const TabMenuButton = ({name,keyValue, callback, isActive }) => {
   
    return(
        <button
            onClick={() => callback(keyValue)}
            className={`btn btn-tab ${isActive ? 'active' : ''}`}
        >
            {name}
        </button>
    );
}
export default TabMenuButton;