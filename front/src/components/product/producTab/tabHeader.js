'use client'
import { useState } from "react";
import TabMenuButton from "./tabMenuButton";
const TabHeader = ({data, callback}) =>{
    const [activeKey, setActiveKey] = useState('competitors-list');

    const buttonCallback = (key) => {
        setActiveKey(key)
        callback(key)
    }
    return ( 
        data.map((item, index) => (
            <TabMenuButton
                key={index}
                name={item.name}
                keyValue={item.key}
                callback={buttonCallback}
                isActive={item.key === activeKey} // Pass whether this button is active
            />
        ))
    );
}
export default TabHeader;