'use client'
import { useState } from "react";
import TabHeader from "./tabHeader";
import TabBody from "./tabBody";
import '@/styles/scss/components/productTabs.scss';
const ProductTab = ({data}) => {
    const [activeTab, setActiveTab] = useState('competitors-list');
    const buttonsData = [
        {name: 'Competitors List', key: 'competitors-list'},
        {name: 'Market Position', key:'market-position'},
        {name: 'Comparison', key:'comparison'},
        {name: 'Predictive Insights', key:'predictive-insight'},
        {name: 'Custom Notes', key:'custom-notes'},
        {name: 'Product Benchmark', key:'product-benchmark'},
        {name: 'Reports', key:'reports'},
    ]
    const activeTabHandler = (key) =>{ 
        setActiveTab(key);
    }
    return(
        <div className="product-tab">
            <div className="product-tab--header">
                <TabHeader data={buttonsData} callback={activeTabHandler}/>
            </div>
            <div className="product-tab--body">
                <TabBody activeTab={activeTab} data={data}/>
            </div>
        </div>
    );
}
export default ProductTab;