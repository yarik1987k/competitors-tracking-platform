import { useState, useEffect, useRef } from "react";
import '@/styles/scss/components/dropDown.scss';

const PricingDropDown = ({ data, callBack }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(data[0].name);  // Default to first option
    const dropdownRef = useRef(null);

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                setIsOpen(false);
            }
        };

        document.addEventListener("click", handleOutsideClick);

        return () => {
            document.removeEventListener("click", handleOutsideClick);
        };
    }, []);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const selectItemHandler = (item) => {
        setIsOpen(false);
        setSelectedOption(item.name);
        callBack(item.value);
    };

    const options = data.map(item => (
        <li key={item.value}>
            <button onClick={() => selectItemHandler(item)}>
                {item.name}
            </button>
        </li>
    ));

    return (
        <div className="drop-down" ref={dropdownRef}>
            <button className="defualt btn" onClick={toggleDropdown}>
                <span className="label">{selectedOption}</span>
                <span className="icon">
                    {isOpen ? (
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="m4.5 15.75 7.5-7.5 7.5 7.5" />
                        </svg>
                    ) : (
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="m19.5 8.25-7.5 7.5-7.5-7.5" />
                        </svg>
                    )}
                </span>
            </button>
            {isOpen && (
                <div className="drop-down--list">
                    <ul>
                        {options}
                    </ul>
                </div>
            )}
        </div>
    );
};

export default PricingDropDown;
