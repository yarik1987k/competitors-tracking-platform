"use client"
import { Line } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js';
import 'chartjs-adapter-date-fns';  // Import the adapter
import { enUS } from 'date-fns/locale';  // Import the locale
import { useState, useMemo } from 'react';
import { subDays, subMonths, parseISO } from 'date-fns';
import PricingDropDown from './pricingDropDown';
import '@/styles/scss/components/chart.scss';
Chart.register(...registerables);

function generateColor(index, total) {
    // Base HSL values for the three colors
    const baseColors = [
        { h: 285, s: 45, l: 59 },  // Focus color in HSL
        { h: 180, s: 97, l: 43 },  // Success color in HSL
        { h: 260, s: 11, l: 55 }   // Disable color in HSL
    ];

    // Calculate new hue by rotating slightly based on the index
    const colorIndex = index % baseColors.length; // Ensure we select a base color based on the index
    let hueShift = (2 / total) * (index / baseColors.length); // Smaller shift as more colors are needed
    let newHue = (baseColors[colorIndex].h + hueShift) % 360; // Wrap around the color wheel

    // You can also adjust lightness by a small percentage if needed
    let newLightness = baseColors[colorIndex].l + (index % 2 === 0 ? -5 : 5); // Alternately darken and lighten
    newLightness = Math.max(10, Math.min(90, newLightness)); // Clamp values to valid lightness range

    return `hsl(${newHue}, ${baseColors[colorIndex].s}%, ${newLightness}%)`;
}


function getLatestDate(data) {
    let latestDate = new Date(0); // Start with the oldest possible date
    Object.keys(data).forEach(key => {
        data[key].forEach(item => {
            const itemDate = new Date(item.date);
            if (itemDate > latestDate) {
                latestDate = itemDate;
            }
        });
    });
    return latestDate;
}


const ProductPricingOverview = ({ data }) => {

    const dateRanges = [
        { name: 'Last 7 Days', value: '7days' },
        { name: 'Last Month', value: 'month' }
    ];

    const [timeFrame, setTimeFrame] = useState(dateRanges[0].value); 

    const endDate = useMemo(() => getLatestDate(data), [data]); // Compute the latest date whenever the data changes

    
    const filteredData = useMemo(() => {
        const startDate = timeFrame === '7days' ? subDays(endDate, 7) : subMonths(endDate, 1);

        const filtered = {};
        Object.keys(data).forEach(key => {
            filtered[key] = data[key].filter(d => parseISO(d.date) >= startDate && parseISO(d.date) <= endDate);
        });
        return filtered;
    }, [timeFrame, data, endDate]);

    const currentLabel = useMemo(() => {
    const item = dateRanges.find(dr => dr.value === timeFrame);
    return item ? item.name : '';
}, [timeFrame]);

const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
        legend: {
            display: true,
            position: 'bottom', // Positions the legend at the bottom of the chart
            align: 'start', // Aligns the legend items to the start (left)
            labels:{
               
                usePointStyle: true, // Ensures the legend uses the same style as the data points
                useBorderRadius: true,
                pointStyleWidth: 17,
            }
        },
        tooltip: {
            // Tooltips configuration, if you need any specific settings
        }
    },
    scales: {
        x: {
            type: 'time',
            time: {
                unit: timeFrame === '7days' ? 'day' : 'week',
            },
            adapters: {
                date: {
                    locale: enUS,
                }
            },
            grid:{
                backgroundColor: "#fff"
            }
        },
        y:{

        }
    }
};

    const chartData = {
        labels: filteredData[Object.keys(filteredData)[0]].map(item => item.date),
        datasets: Object.keys(filteredData).map((key, index) => ({
            label: `Competitor ${key}`,
            data: filteredData[key].map(item => item.price),
            fill: false,
            borderColor: generateColor(index, Object.keys(filteredData).length),
            tension: 0.4,
            borderJoinStyle: 'round',
            backgroundColor: generateColor(index, Object.keys(filteredData).length), 
            radius: 3,
            pointStyle: 'circle',

        }))
    };

    return (
        <div className='chart'>
            <div className='chart--header d-flex align-items-center justify-content-between'>
                <div className='chart--header-title'>
                    <h5>Pricing overview</h5>
                </div>
                <div className='chart--buttons d-flex align-items-center justify-content-end'>
                    <div className='sorting d-flex align-items-center'>
                        <span>{currentLabel}</span>
                        <PricingDropDown data={dateRanges} callBack={(value) => setTimeFrame(value)}/>
                    </div> 
                    <button className='btn btn-link'>Set Price Alert</button>
                </div>
            </div>

            
            <Line data={chartData} options={options} /> 
        </div>
    );
};

export default ProductPricingOverview;
