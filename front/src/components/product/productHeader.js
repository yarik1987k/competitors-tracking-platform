import ProductButtons from "./productButtons";
import '@/styles/scss/components/productHeader.scss';
const ProductHeader = ({data}) => {
    
return(
    <div className="product-header">
        <div className="d-flex">
            <div className="col-7">
                <div className="product-header-content d-flex">
                    <div className="product-image">
                        <img src={data.image} className="img-fluid"/>
                    </div>
                    <div className="content ps-3">
                        <h1>{data.name}</h1>
                        <p>{data.description}</p>
                        <div className="content-meta">
                            <ul>
                                <li>
                                    <p><strong>Dated Added</strong>: {data.dateAdded}</p>
                                </li>
                                <li>
                                    <p><strong>Starting Price</strong>: {data.startPrice} {data.currency}</p>
                                </li>
                                <li>
                                    <p><strong>{data.competitors.length > 2 ? 'Competitors' : 'Competitor'}</strong>: {data.competitors.length} </p>
                                </li>
                                <li>
                                    <p><strong>Stock Availability</strong>:</p>
                                </li>                                
                                <li>
                                    <p><strong>Stock Amount</strong>:</p>
                                </li>                                                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-5">
                <ProductButtons id={data.id}/>
            </div>
        </div>
    </div>
)
}
export default ProductHeader;