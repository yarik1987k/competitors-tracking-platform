import React from 'react';
import "@/styles/scss/components/signalMenu.scss";

const SignalMenu = ({ callBack, activeMenu }) => {
    const menuItems = [
        {name: 'Out of Stock Changes', key: 'stock-change' },
        {name: 'Content Changes', key: 'content-change' },
        {name: 'Promotion Changes', key: 'promotion-change' },
        {name: 'Rating Changes', key: 'rating-change' },
        {name: 'SEO Metadata Changes', key: 'seo-change' },
        {name: 'Website Errors', key: 'erros-change' },
        {name: 'Overall Stats', key: 'stats' }
    ];

    return (
        <div className="signal-menu">
            <ul>
                {menuItems.map(item => (
                    <li key={item.name}>
                        <button
                            className={`btn btn-tab ${activeMenu === item.key ? 'active' : ''}`}
                            onClick={() => callBack(item.key, item.name)}
                        >
                            {item.name}
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default SignalMenu;
