import GlobalChart from "@/components/global/globalChart";
import OverallStats from "./overallStats";
import ContentChanges from "./contentChanges";

const SignalContent = ({data, contentKey, contentName}) => {
 
     
    const renderComponentByKey = () => {  
        switch (contentKey) {
            case "stats":
                return <OverallStats title={contentName} competitorId={data.id}/>;
            case "content-change":
                return <ContentChanges title={contentName}  data={data} competitorId={data.id}/>;
            default:
                return <GlobalChart data={data} chartTitle={contentName}/>;
        }
    }; 

    return(
        <div className="signal-content">
            {renderComponentByKey()}
        </div>
    );
}

export default SignalContent;
