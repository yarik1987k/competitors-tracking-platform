import signalsData from '@/data/competitorsDataSignals.json';
import DoughnutChart from '@/components/global/doughnutChart';
import ProgressBar from '@/components/global/progressBar';
import "@/styles/scss/components/overallStats.scss";
const OverallStats = ({competitorId, title}) =>{

    const data = signalsData.find(item => item.competitorId === competitorId);
    return(
        <div className='overall-stats ps-5'>
            <div className='overall-stats--title'>
                <h5>{title}</h5>
            </div> 
            <div className='row justify-content-start'>
                <div className='col-auto me-5'>
                    <DoughnutChart data={data}/>
                </div>
                <div className='col-auto d-flex justify-content-end'>
                    <div className='overall-stats--bars'>
                        <ProgressBar label="Out of Stock Changes" value={data?.outOfStockChanges} />
                        <ProgressBar label="Content Changes" value={data?.contentChanges} />
                        <ProgressBar label="Promotion Changes" value={data?.promotionChanges} />
                        <ProgressBar label="Price Changes" value={data?.priceChanges} />
                        <ProgressBar label="Rating Changes" value={data?.ratingChanges} />
                        <ProgressBar label="SEO Metadata Changes" value={data?.seoMetaDataChanges} />
                        <ProgressBar label="Website Errors" value={data?.websiteErrors} />    
                    </div>                
                </div>    
            </div>
        </div>
    )
}
export default OverallStats;