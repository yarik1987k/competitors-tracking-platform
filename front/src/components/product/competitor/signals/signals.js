import { useState } from 'react';
import "@/styles/scss/components/signals.scss";
import SignalMenu from "./signalMenu";
import SignalContent from './signalContent';

const Signals = ({ data }) => {
    // Initial state could be the first menu item or a specific one if needed
    const [activeMenu, setActiveMenu] = useState('stock-change');
    const [activeMenuName, setActiveMenuName] = useState('Out of Stock Changes');

    const handleMenuChange = (menuKey, menuName) => {
        setActiveMenu(menuKey);
        setActiveMenuName(menuName)
    };

    return (
        <div className="signal-block">
            <div className="signal-block--grid">
                <div className="left">
                    <SignalMenu competitorId={data.id} callBack={handleMenuChange} activeMenu={activeMenu}/>
                </div>
                <div className='right'>
                    <SignalContent data={data} contentKey={activeMenu} contentName={activeMenuName}/>
                </div>
            </div>
        </div>
    );
};

export default Signals;
