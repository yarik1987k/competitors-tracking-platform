import GlobalChart from "@/components/global/globalChart";
import dataList from "@/data/competitrosDataList.json";
import DataList from "@/components/global/dataList";
const ContentChanges = ({data, competitorId, title}) => {
   
    return(
        <div className="content-changes"> 
            <div className='row justify-content-start'>
                <div className='col-7'>
                <GlobalChart data={data} chartTitle={title}/>
                </div>
                <div className='col-5 d-flex justify-content-start'>
                    <div className='content-changes--list w-100'>
                          <DataList competitorId={competitorId} data={dataList}/>
                    </div>                
                </div>    
            </div>
        </div>
    );
}
export default ContentChanges;