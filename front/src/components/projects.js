'use client'
import React, { useState } from 'react';
import Filter from './global/filter';
import Sorting from './global/sorting';
import SingleProjectList from './singleProjectList';


const Projects = ({ data }) => {
    const [searchResults, setSearchResults] = useState(data.projects || []);
    const [activeMenuId, setActiveMenuId] = useState(null); 
    const sortingNames = [
        {name: "Project Name"},
        {name: "Project Status"}
    ]
    
    const toggleMenu = (id) => {
        if (activeMenuId === id) {
            setActiveMenuId(null);   
        } else {
            setActiveMenuId(id);    
        }
    };
    const filterHandler = (results) => {
        setSearchResults(results)
    }
    const sortingHandler = (sortKey) => {
        const sortedResults = [...searchResults].sort((a, b) => {
            const valueA = (a[sortKey] || '').toLowerCase(); // Fallback to empty string if undefined
            const valueB = (b[sortKey] || '').toLowerCase(); // Fallback to empty string if undefined
    
            if (valueA < valueB) return -1;
            if (valueA > valueB) return 1;
            return 0;
        });
    
        setSearchResults(sortedResults);
    };
    
    const selectionHandler = (value) => {
        // TODO: Make selection handler
        console.log(value)
    }
     
    return (
        <section className='list'>
            <Filter data={data.projects} callBack={filterHandler}/>
            <Sorting selectionCallBack={selectionHandler} callBack={sortingHandler} names={sortingNames}/>
            {searchResults.map(project => (
                <SingleProjectList toggler={toggleMenu} isActive={activeMenuId === project.id} data={project} key={project.id}/>
            ))}
        </section>
    );
}

export default Projects;
