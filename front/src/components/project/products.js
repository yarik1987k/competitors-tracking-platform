'use client'
import React, { useState } from 'react';
import SingleProductList from './singleProductList';
import Filter from '@/components/global/filter';
import Sorting from '@/components/global/sorting';
 
const Products = ({data}) => {
    const [searchResults, setSearchResults] = useState(data);
    const [activeMenuId, setActiveMenuId] = useState(null); 
    const sortingNames = [
        {name: "Product Name"},
        {name: "Product Status"}
    ] 
    const toggleMenu = (id) => {
        if (activeMenuId === id) {
            setActiveMenuId(null);   
        } else {
            setActiveMenuId(id);    
        }
    };
    const filterHandler = (results) => {
        setSearchResults(results)
    }
    const sortingHandler = (sortKey) => {
        const sortedResults = [...searchResults].sort((a, b) => { 
            if (a[sortKey].toLowerCase() < b[sortKey].toLowerCase()) return -1;
            if (a[sortKey].toLowerCase() > b[sortKey].toLowerCase()) return 1;
            return 0;
        });

        setSearchResults(sortedResults); 
    };
    const selectionHandler = (value) => {
        console.log(value)
    }
    return(
        <section className='list'>
            <Filter data={data} callBack={filterHandler}/>
            <Sorting selectionCallBack={selectionHandler} callBack={sortingHandler} names={sortingNames}/>
            {searchResults.map(project => (
                <SingleProductList toggler={toggleMenu} isActive={activeMenuId === project.id} data={project} key={project.id}/>
            ))}
        </section>
    );
}
export default Products;