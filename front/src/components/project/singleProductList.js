
import SmallMenu from "@/components/global/smallMenu";
import '@/styles/scss/components/list.scss';
const SingleProductList = ({ data, toggler, isActive }) => {
     
    const menuItems = [
        { label: 'Edit', action: 'edit' },
        { label: 'Delete', action: 'delete' }, 
        { label: 'Change Status', action: 'change-status' }
    ];
    
    const menuCallBack = (key) => {
        toggler(data.id);
        console.log(key)
    }; 

    const menuToggler = (event) => {
        event.stopPropagation();   
        toggler(data.id);
    }; 
    return(
        <div className="single-list">
            <div className='single-list-container'>
                <div className='selection'>
                    <input type='checkbox' value={data.id} />
                </div>    
                <div className="d-flex justify-content-start ps-5 row row-cols-2">
                <div className='single-col'>
                    <div className='single-col-content'>
                        <a href={`/dashboard/product/${data.id}`}>{data.name}</a>
                        <p>{data.description}</p>
                    </div>
                </div> 
                <div className='single-col status-col'>
                    <span className='list-status'><span className="badge">{data.status}</span></span>
                </div>
                <div className='single-col single-func'>
                    <div className='d-flex justify-content-end column-gap-3'>
                        <span className='list-amount'><span className="badge">{data.competitors.length} {data.competitors.length > 1 ? 'competitors' : 'competitor'} </span></span>
                        <div className='list-options'>
                            <button onClick={menuToggler}>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5ZM12 12.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5ZM12 18.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5Z" />
                                </svg>
                            </button>
                        </div>
                    </div>
                    {isActive && <SmallMenu items={menuItems}  onItemSelected={menuCallBack} />}
                </div> 
                </div>                               
            </div>
        </div>
    )
}
export default SingleProductList;