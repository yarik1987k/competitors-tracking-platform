const Header = ({data}) => {
    return(
        <div className="header-project">
            <h1>{data[0].name}</h1>
            <p>{data[0].description}</p>
        </div>
    )
}
export default Header;