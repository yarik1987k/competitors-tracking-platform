"use client"
import { Line } from 'react-chartjs-2';
import { Chart, registerables } from 'chart.js';
import 'chartjs-adapter-date-fns';  // Import the adapter
import { enUS } from 'date-fns/locale';  // Import the locale
import { subDays, subMonths, parseISO } from 'date-fns';
import PricingDropDown from '../product/pricingDropDown';
import "@/styles/scss/components/chart.scss";
import { useState, useMemo } from 'react';


function generateColor(index, total) {
    // Base HSL values for the three colors
    const baseColors = [
        { h: 285, s: 45, l: 59 },  // Focus color in HSL
        { h: 180, s: 97, l: 43 },  // Success color in HSL
        { h: 260, s: 11, l: 55 }   // Disable color in HSL
    ];

    // Calculate new hue by rotating slightly based on the index
    const colorIndex = index % baseColors.length; // Ensure we select a base color based on the index
    let hueShift = (2 / total) * (index / baseColors.length); // Smaller shift as more colors are needed
    let newHue = (baseColors[colorIndex].h + hueShift) % 360; // Wrap around the color wheel

    // You can also adjust lightness by a small percentage if needed
    let newLightness = baseColors[colorIndex].l + (index % 2 === 0 ? -5 : 5); // Alternately darken and lighten
    newLightness = Math.max(10, Math.min(90, newLightness)); // Clamp values to valid lightness range

    return `hsl(${newHue}, ${baseColors[colorIndex].s}%, ${newLightness}%)`;
}

function getLatestDate(data) {
    if(!data) return new Date(0);
    let latestDate = new Date(0); // Start with the oldest possible date
    Object.keys(data).forEach(key => {
        if (Array.isArray(data[key])) {
            data[key].forEach(item => {
                const itemDate = new Date(item.date);
                if (itemDate > latestDate) {
                    latestDate = itemDate;
                }
            });
        } else {
            console.error(`Expected an array for key ${key}, but received:`, data[key]);
        }
    });
    
    return latestDate;
}

const GlobalChart = ({data, chartTitle}) => {
    const dateRanges = [
        { name: 'Last 7 Days', value: '7days' },
        { name: 'Last Month', value: 'month' }
    ];
    const [timeFrame, setTimeFrame] = useState(dateRanges[0].value); 

    const endDate = useMemo(() => getLatestDate(data), [data]); // Compute the latest date whenever the data changes

    
    const filteredData = useMemo(() => {
        if (!data) return {};
        const startDate = timeFrame === '7days' ? subDays(endDate, 7) : subMonths(endDate, 1);
    
        const filtered = {};
        Object.keys(data).forEach(key => {
            if (Array.isArray(data[key])) {
                filtered[key] = data[key].filter(d => {
                    // Check if 'date' exists and is a valid string
                    if (typeof d.date === 'string' && d.date.trim() !== '') {
                        try {
                            const parsedDate = parseISO(d.date);
                            return parsedDate >= startDate && parsedDate <= endDate;
                        } catch (error) {
                            console.error('Error parsing date:', d.date, 'Error:', error);
                            return false;
                        }
                    } else {
                        console.error('Invalid or missing date:', d);
                        return false;
                    }
                });
            }
        });
        return filtered;
    }, [timeFrame, data, endDate]);
    
    
    

    const currentLabel = useMemo(() => {
    const item = dateRanges.find(dr => dr.value === timeFrame);
    return item ? item.name : '';
}, [timeFrame]);

const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
        legend: {
            display: true,
            position: 'bottom', // Positions the legend at the bottom of the chart
            align: 'start', // Aligns the legend items to the start (left)
            labels:{
               
                usePointStyle: true, // Ensures the legend uses the same style as the data points
                useBorderRadius: true,
                pointStyleWidth: 17,
            }
        },
        tooltip: {
            // Tooltips configuration, if you need any specific settings
        }
    },
    scales: {
        x: {
            type: 'time',
            time: {
                unit: timeFrame === '7days' ? 'day' : 'week',
            },
            adapters: {
                date: {
                    locale: enUS,
                }
            },
            grid:{
                backgroundColor: "#fff"
            }
        },
        y:{

        }
    }
};
const chartData = useMemo(() => {
    // Make sure there is at least one key and that key has data
    const firstKey = Object.keys(filteredData)[0];
    if (!firstKey || !filteredData[firstKey].length) {
        return { labels: [], datasets: [] }; // Return an empty chart structure
    }

    return {
        labels: filteredData[firstKey].map(item => item.date),
        datasets: Object.keys(filteredData).map((key, index) => ({
            label: `Competitor ${key}`,
            data: filteredData[key].map(item => item.price),
            fill: false,
            borderColor: generateColor(index, Object.keys(filteredData).length),
            tension: 0.4,
            borderJoinStyle: 'round',
            backgroundColor: generateColor(index, Object.keys(filteredData).length), 
            radius: 3,
            pointStyle: 'circle',
        }))
    };
}, [filteredData]);


return(
    <div className='chart'>
    <div className='chart--header d-flex align-items-center justify-content-between'>
        {chartTitle &&
            <div className='chart--header-title'>
                <h5>{chartTitle}</h5>
            </div>
        }
        <div className='chart--buttons d-flex align-items-center justify-content-end'>
            <div className='sorting d-flex align-items-center'>
                <span>{currentLabel}</span>
                <PricingDropDown data={dateRanges} callBack={(value) => setTimeFrame(value)}/>
            </div>  
        </div>
    </div>

    
    <Line data={chartData} options={options} /> 
</div>
)
}
export default GlobalChart;