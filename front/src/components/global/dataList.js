import { useState } from 'react';
import '@/styles/scss/components/dataList.scss';

const DataList = ({ data, competitorId }) => {
    const [showPopup, setShowPopup] = useState(false);
    const [currentImage, setCurrentImage] = useState('');

    const competitorData = data.find(item => item.competitorId === competitorId);
    if (!competitorData) {
        return <div>No data found for competitorId: {competitorId}</div>;
    }

    const handleImageClick = (url) => {
        setCurrentImage(url);
        setShowPopup(true);
    };

    const closePopup = () => {
        setShowPopup(false);
    };

    return(
        <div className="data-list">
            <div className='data-list--title pb-3'>
                <h5> Content changes screenshots </h5>
            </div>
            {competitorData.dataList.map(item => (
                <div className="d-flex align-items-center justify-content-start mb-3" key={item.id}>
                    <div className="data-list--thumbnail me-3">
                        <img src={item.screenShotUrl} alt="Screenshot" className='img-fluid' onClick={() => handleImageClick(item.screenShotUrl)} />
                    </div>
                    <div className="data-list--date  me-3">
                        <p>Date added: {item.date}</p>
                    </div>
                    <div className="data-list--link">
                        <button className='btn btn-link'  onClick={() => handleImageClick(item.screenShotUrl)} >
                            View Screenshot
                        </button> 
                    </div>
                </div>
            ))}
            {showPopup && (
                <div className="popup">
                    <div className="popup-content">
                        <div className='close'>
                        <button className="btn btn--icon" onClick={closePopup}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
                            </svg>
                        </button> 
                        </div>
                        <img src={currentImage} alt="Full Screenshot" />
                    </div>
                </div>
            )}
        </div>
    )
}
export default DataList;
