import { useState } from "react";
import '@/styles/scss/components/searchInput.scss';
const SearchFilter = ({data,callBack}) => {
    const [ searchValue, setSearchValue ] = useState(''); 
    const searchHandler = (e) => {
        const value = e.target.value;
        setSearchValue(value);
 
        callBack(value)
    }
    
    return(
    <div className="search-input">
        <input className="search-input--input" type="text" placeholder="Search by name or id" onChange={searchHandler} value={searchValue}/>    
    </div>
    )
    
}
export default SearchFilter;