import { Doughnut } from 'react-chartjs-2';
import 'chart.js/auto'; 
import '@/styles/scss/components/doughnutChart.scss';

const centerTextPlugin = {
    id: 'centerText',
    beforeDraw: (chart) => {
        const width = chart.width;
        const height = chart.height;
        const ctx = chart.ctx;
        ctx.restore();

        const overRallActivity = chart.options.overRallActivity + '%' || '0%'; // Text to display
        const labelLine1 = "Competitor"; // First line of the label
        const labelLine2 = "Activity"; // Second line of the label

        // Set font size for the percentage
        let fontSize = (height / 150).toFixed(2);
        ctx.font = fontSize + "em sans-serif";
        ctx.textBaseline = "middle";

        // Adjust textY to move the percentage and labels upwards
        const textY = height / 2 - parseFloat(fontSize) * 5; // Move text up

        // Calculate position and draw the percentage
        const textX = Math.round((width - ctx.measureText(overRallActivity).width) / 2);
        ctx.fillText(overRallActivity, textX, textY);

        // Set font size for the label, smaller than the percentage
        ctx.font = (fontSize * 0.3) + "em sans-serif"; // Smaller font size for the label
        const labelY1 = textY + parseFloat(fontSize) * 15; // Position the first line of the label below the percentage
        const labelX1 = Math.round((width - ctx.measureText(labelLine1).width) / 2);
        ctx.fillText(labelLine1, labelX1, labelY1);

        // Position the second line of the label slightly below the first
        const labelY2 = labelY1 + parseFloat(fontSize) * 5; // Adjust vertical space between lines
        const labelX2 = Math.round((width - ctx.measureText(labelLine2).width) / 2);
        ctx.fillText(labelLine2, labelX2, labelY2);

        ctx.save();
    }
};



const DoughnutChart = ({data}) => {
    const chartData = {
        labels: [],
        datasets: [{
            data: [parseInt(data?.overRallActivity || '0%'), 100 - parseInt(data?.overRallActivity || '0%')],
            backgroundColor: ['#05D1D1', '#F3F0F9'],
            borderWidth: 0
        }]
    };

    const options = {
        events: [],  // Disable all interactive events like mouse hover and clicks
        plugins: {
            legend: {
                display: true,
                position: 'top'
            },
            tooltip: {
                enabled: false  // Optionally disable tooltips
            }
        },
        // Pass overRallActivity to the options so it can be accessed by the plugin
        overRallActivity: data?.overRallActivity
    };

    return (
        <div className='doughnut-chart'> 
            {data ? (
                <div className='doughnut-chart--container'>
                    <h3>Overall competitor activity</h3>
                    <p>Total stats based on gathered data</p>
                    <div className='doughnut-chart--chart'>
                        <Doughnut data={chartData} options={options} plugins={[centerTextPlugin]} />
                    </div>
                </div>
            ) : (
                <p>No data available for this competitor.</p>
            )}
        </div>
    );
};

export default DoughnutChart; 