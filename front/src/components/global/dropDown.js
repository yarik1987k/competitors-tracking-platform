import { useState, useEffect, useRef } from "react";
import '@/styles/scss/components/dropDown.scss';

const DropDown = ({ data, callBack }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState('Any status');
    const dropdownRef = useRef(null);

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                setIsOpen(false);
            }
        };

        document.addEventListener("click", handleOutsideClick);

        return () => {
            document.removeEventListener("click", handleOutsideClick);
        };
    }, []);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const selectItemHandler = (value) => {
        setIsOpen(false);
        setSelectedOption(value);
        if (value !== '') {
            callBack(value);
        }
    };

    const uniqueOptions = Array.from(new Set(data.map(item => item.name)))
        .map(name => ({
            name,
            value: data.find(item => item.name === name).value
        })); 

        const options = [
            <li key="any-status">
                <button onClick={() => selectItemHandler('Any option')}>
                Any option
                </button>
            </li>,
            ...uniqueOptions.map(item => (
                <li key={item.name}>
                    <button onClick={() => selectItemHandler(item.name)}>
                        {item.name}
                    </button>
                </li>
            ))
        ];

    return (
        <div className="drop-down" ref={dropdownRef}>
            <button className="defualt btn" onClick={toggleDropdown}>
                <span className="label">{selectedOption}</span>
                <span className="icon">
                    {isOpen ? (
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="m4.5 15.75 7.5-7.5 7.5 7.5" />
                        </svg>
                    ) : (
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="m19.5 8.25-7.5 7.5-7.5-7.5" />
                        </svg>
                    )}
                </span>
            </button>
            {isOpen && (
                <div className="drop-down--list">
                    <ul>
                        {options}
                    </ul>
                </div>
            )}
        </div>
    );
};

export default DropDown;
