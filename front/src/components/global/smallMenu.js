import { useEffect, useRef } from 'react';
import '@/styles/scss/components/SmallMenu.scss';

const SmallMenu = ({ items, onItemSelected }) => {
    const menuRef = useRef(null);

    const menuHandler = (action, event) => {  
        event.preventDefault();
        onItemSelected(action);
    };

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (menuRef.current && !menuRef.current.contains(event.target)) {
                onItemSelected(null);  
            }
        };

        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [onItemSelected]);

    return (
        <div ref={menuRef} className="small-menu">
            <ul>
                {items.map((item, index) => (
                    <li key={index}>
                        <button onClick={(e) => menuHandler(item.action, e)}>
                            <span>{item.label}</span>
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default SmallMenu;
