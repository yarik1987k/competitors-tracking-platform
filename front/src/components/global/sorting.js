import '@/styles/scss/components/sorting.scss';
const Sorting = ({callBack, selectionCallBack, names}) => {
    const handleCheckboxChange = (event) => {
        selectionCallBack(event.target.checked ? event.target.value : null);
    };
     
     
    return(
        <div className={`sorting`}>
                <div className="select-option">
                    <input type="checkbox" value='all' onChange={handleCheckboxChange}/>
                </div>  
            <div className={`d-flex justify-content-start ps-5 row row-cols-${names.length}`}>
                {names.map((name, index) => {
                    return( 
                        <div className="single-sort d-flex col" key={index}> 
                            <button onClick={() => callBack(name.name)}>
                                <span>{name.name}</span>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M8.25 15 12 18.75 15.75 15m-7.5-6L12 5.25 15.75 9" />
                                </svg>
                            </button>
                        </div>
                    )
                })} 
            </div> 
        </div>
    );
}
export default Sorting;