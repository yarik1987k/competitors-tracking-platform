import { useState, useEffect } from "react";
import DropDown from "./dropDown";
import SearchFilter from "./searchFilter";
import '@/styles/scss/components/filters.scss';

const Filter = ({ data, callBack }) => {
    const [searchQuery, setSearchQuery] = useState('');
    const [statusFilter, setStatusFilter] = useState('');

    useEffect(() => {
        applyFilters(); 
    }, [searchQuery, statusFilter]);
 
    const statuses = [...new Set(data.map(item => item.status))].map(status => ({
        value: status,
        name: status
    }));
 
    const dropDownCallback = (status) => {
        setStatusFilter(status);
    };
 
    const searchCallback = (query) => {
        setSearchQuery(query.toLowerCase());
    };
 
    const applyFilters = () => {
        const filteredData = data.filter(item => {
            if(statusFilter === 'Any option'){
                return item.name.toLowerCase().includes(searchQuery)
            }
            if(statusFilter === 'Any option' & searchQuery === ''){
                return data;
            }
            return (item.name.toLowerCase().includes(searchQuery) && (statusFilter ? item.status === statusFilter : true));
        });
        callBack(filteredData);
    };

    return (
        <div className="filters py-5">
            <div className="d-flex w-100">
                <div className="filter w-auto me-4">
                    <SearchFilter data={data} callBack={searchCallback} />
                </div>
                <div className="filter w-auto">
                    <DropDown data={statuses} callBack={dropDownCallback} />
                </div>
            </div>
        </div>
    );
};

export default Filter;
