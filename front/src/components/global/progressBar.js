import '@/styles/scss/components/progressBar.scss';
const ProgressBar = ({ label, value, max = 100 }) => {
    return (
        <div className="single-progress-bar">
            <div className='progress-label'> <label>{label}</label></div>
            <div className="progress"> 
                <div className="progress-bar" role="progressbar" style={{ width: `${value}%` }} aria-valuenow={value} aria-valuemin="0" aria-valuemax={max}>
                   {value}%
                </div>
            </div>
        </div>
    );
}
export default ProgressBar;