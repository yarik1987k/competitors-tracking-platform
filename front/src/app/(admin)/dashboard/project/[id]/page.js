"use client"

import projectsData from "@/data/projectsData.json";
import productsData from "@/data/productsData.json";
import Header from "@/components/project/header";
import Products from "@/components/project/products";

const Project = ({params}) => {
  
  const productDataFilter = productsData.filter(product => product.projectId === parseInt(params.id))
  const projectDataFilter = projectsData.projects.filter(project => project.id === parseInt(params.id))
 
  return(
    <div className="single-project">
      <Header data={projectDataFilter}/>
      <Products data={productDataFilter}/> 
    </div>
    
  )
}
export default Project;