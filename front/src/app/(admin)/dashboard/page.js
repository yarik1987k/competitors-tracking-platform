import projectsData from "@/data/projectsData.json";
import Projects from "@/components/projects";

export default function Dashboard() {
  return (
    <div className="projects">
      <h1>Projects</h1>
      <Projects data={projectsData}/>
    </div>
    
  );
}
