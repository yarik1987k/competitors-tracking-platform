import productsData from "@/data/productsData.json";
import competitorsData from "@/data/competitorsData.json";
import ProductHeader from "@/components/product/productHeader";
import ProductPricingOverview from "@/components/product/productPricingOverview";
import PricingData from '@/data/pricingDemoData.json';
import ProductTab from "@/components/product/producTab/productTab";
export default function ProductId({ params }) {
 
  const product = productsData.find(product => product.id === parseInt(params.id));
  const competitorIds = product.competitors.map(competitor => competitor.id);
  const filteredCompetitors = competitorsData.filter(competitor => competitorIds.includes(competitor.id));
  
 
  return (
    <section className="single-product pt-5">
      <ProductHeader data={product}/>
      <ProductPricingOverview data={PricingData}/> 
      <ProductTab data={filteredCompetitors} /> 
      
    </section>
  );
}
