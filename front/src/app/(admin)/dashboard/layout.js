import '@/styles/scss/dashboard/main.scss';
import Sidebar from '@/components/sidebar';
export const metadata = {
    title: "Dashboard",
    description: "Main Dashbaord page of the app",
  };

const DashboardLayout = ({children}) =>{
    
    return (
        <section className="dashboard d-flex">
          <div className='container-fluid px-0'>
            <div className='row mx-0 h-100'>
              <div className='col-2 px-0'>
                <Sidebar/>
              </div>
              <div className='col-10 pt-5'> 
                {children}
              </div>
            </div>
          </div>
        </section>
      );
}
export default DashboardLayout;