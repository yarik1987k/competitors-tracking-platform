/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['tailwindui.com','mages.unsplash.com'],
      },
};

export default nextConfig;
