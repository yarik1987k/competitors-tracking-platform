import { getPricingById, getPricings, deletePricingById, createPricing } from '../db/pricing.js';
export const getAllPricings = async (req, res) => {
    try {
        const pricings = await getPricings();
        return res.status(200).json(pricings);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);        
    }
}
export const deletePricing = async (req, res) => {
    try {
        const {id} = req.params;
        const deletedPricing = await deletePricingById(id);
        return res.json(deletedPricing);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}
export const createNewPricing = async (req, res) => {
    try {
        const {competitorId, price, createdAt, createdTime } = req.body;
        if(!competitorId || !price || !createdTime || !createdAt){
            return res.sendStatus(400);
        }
        const createdPricing = await createPricing(req.body);
        return res.json(createdPricing);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}

export const getSinglePricing = async (req, res) => {
    try {
        const {id} = req.params; 

      if(!id){
        return res.sendStatus(400);
      }
      const pricing = await getPricingById(id);
      return res.status(200).json(pricing);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }

}