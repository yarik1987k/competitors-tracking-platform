
import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import { extractPriceAndCurrency } from '../helpers/scraping.js';
import { createPricing } from '../db/pricing.js';

export const Scraping = async (req, res) => {
    try {
        const {name, competitorId, url, selector, selectorType} =  req.body;
        if(!name && !competitorId || !url || !selector || !selectorType){
            return res.sendStatus(400);
        }
        puppeteer.use(StealthPlugin());
        const userAgentList = [
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1900.70',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/117.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.0 Safari/605.1.15',
            //
        ];
 
        const scrapper = async (url, competitorId, selector, selectorType) => {
            const browser = await puppeteer.launch({
                headless: true,
                args: ['--no-sandbox', '--disable-setuid-sandbox'],  
            });
            const page = await browser.newPage();
            const randomUserAgent = userAgentList[Math.floor(Math.random() * userAgentList.length)];
            try {
                await page.setExtraHTTPHeaders({
                    'Accept-Language': 'en-US,en;q=0.9',
                });
                await page.setUserAgent(randomUserAgent);
                await page.setViewport({ width: 1366, height: 768 });
                await new Promise(resolve => setTimeout(resolve, 1000)); 
                await page.goto(url); 
                await new Promise(resolve => setTimeout(resolve, 2000));
                await page.screenshot({ path:  'bubu-3.jpg' });
                console.log('page', page);
                let element;
                
                if (selectorType === 'xpath') {
                    await page.waitForXPath(selector, { timeout: 60000 });
                    const [elementHandle] = await page.$x(selector);
                    element = elementHandle;   
                } else if (selectorType === 'id') {
                    await page.waitForSelector(`#${selector}`,{ timeout: 60000 });
                    element = await page.$(`#${selector}`);
                } else if (selectorType === 'class') {
                    await page.waitForSelector(`.${selector}`,{ timeout: 60000 });
                    element = await page.$(`.${selector}`);
                } else {
                    throw new Error('Invalid selector type. Supported types are: xpath, id, class.');
                }

                const price = element ? await page.evaluate(el => el.innerText, element) : null;
                if (price) {
                    const [extractedPrice, currency] = extractPriceAndCurrency(price);
        
                    if (extractedPrice !== null) { 
                        return extractedPrice;
                    } else {
                        console.log('Failed to extract price and currency.');
                        return null;
                    }
                } else {
                    console.log('No price found in the first fold.');
                    return null;
                }
            } catch (error) {
                console.error('Error during scraping:', error);
                return null;
            } finally { 
                try {
                    await browser.close();
                } catch (closeError) {
                    console.error('Error during browser close:', closeError);
                }
            }
        }
        const scrapedData = await scrapper(url,competitorId, selector, selectorType);
        console.log('Scraped Data:', scrapedData);
        return res.sendStatus(scrapedData); 
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}