import { getProductById, getProducts, deleteProductById, createProduct } from '../db/products.js';
export const getAllProducts = async (req, res) => {
    try {
        const products = await getProducts();
        return res.status(200).json(products);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);        
    }
}
export const deleteProduct = async (req, res) => {
    try {
        const {id} = req.params;
        const deletedProduct = await deleteProductById(id);
        return res.json(deletedProduct);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}
export const createNewProduct = async (req, res) => {
    try {
        const {name, description, projectId, currency, createdAt } = req.body;
        if(!name || !description || !projectId || !createdAt || !currency ){
            return res.sendStatus(400);
        }
        const createdProduct = await createProduct(req.body);
        return res.json(createdProduct);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}

export const updatedProduct = async (req, res) => {
    try {
        const {id} = req.params;
        const {description} = req.body;

      if(!description){
        return res.sendStatus(400);
      }
      const project = await getProductById(id);
      project.description = description;
      await project.save();

      return res.status(200).json(project).end();
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }

}