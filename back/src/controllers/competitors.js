import { getCompetitorById, getCompetitors, deleteCompetitorById, createCompetitor } from '../db/competitors.js';
export const getAllCompetitors = async (req, res) => {
    try {
        const competitors = await getCompetitors();
        return res.status(200).json(competitors);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);        
    }
}
export const deleteCompetitor = async (req, res) => {
    try {
        const {id} = req.params;
        const deletedCompetitor = await deleteCompetitorById(id);
        return res.json(deletedCompetitor);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}
export const createNewCompetitor = async (req, res) => {
    try {
        const {name, description, productId, currency, createdAt, startPrice, url, selector, selectorType } = req.body;
        if(!name || !description || !productId || !createdAt || !currency || !startPrice || !url || !selector || !selectorType){
            return res.sendStatus(400);
        }
        const createdCompetitor = await createCompetitor(req.body);
        return res.json(createdCompetitor);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}

export const updatedCompetitor = async (req, res) => {
    try {
        const {id} = req.params;
        const {description, url, selector, selectorType, currency, name} = req.body;

      if(!name || !description || !currency || !url || !selector || !selectorType){
        return res.sendStatus(400);
      }
      const competitor = await getCompetitorById(id);
      competitor.description = description;
      competitor.url = url;
      competitor.selector = selector;
      competitor.selectorType = selectorType;
      competitor.currency = currency;
      competitor.name = name;
      await competitor.save();

      return res.status(200).json(project).end();
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }

}