import { getProjectById, getProjects, deleteProjectById, createProject } from '../db/projects.js';
export const getAllProjects = async (req, res) => {
    try {
        const projects = await getProjects();
        return res.status(200).json(projects);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);        
    }
}
export const deleteProject = async (req, res) => {
    try {
        const {id} = req.params;
        const deletedProject = await deleteProjectById(id);
        return res.json(deletedProject);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}
export const createNewProject = async (req, res) => {
    try {
        const {name, description, userId, createdAt } = req.body;
        if(!name || !description || !userId || !createdAt ){
            return res.sendStatus(400);
        }
        const createdProject = await createProject(req.body);
        return res.json(createdProject);
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }
}

export const updatedProject = async (req, res) => {
    try {
        const {id} = req.params;
        const {description} = req.body;

      if(!description){
        return res.sendStatus(400);
      }
      const project = await getProjectById(id);
      project.description = description;
      await project.save();

      return res.status(200).json(project).end();
    } catch (error) {
        console.log(error)
        return res.sendStatus(400);
    }

}