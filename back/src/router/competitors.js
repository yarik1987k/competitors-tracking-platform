import { getAllCompetitors, deleteCompetitor, updatedCompetitor, createNewCompetitor } from '../controllers/competitors.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';

const Competitors = (router) => {
    router.get('/competitors',isAuthenticated, getAllCompetitors);
    router.post('/competitors/new',isAuthenticated, createNewCompetitor);
    router.delete('/competitors/:id',isAuthenticated, isOwner, deleteCompetitor);
    router.patch('/competitors/:id', isAuthenticated, isOwner, updatedCompetitor);  

}

export default Competitors;