import { getAllPricings, deletePricing, createNewPricing, getSinglePricing } from '../controllers/pricing.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';

const Pricings = (router) => {
    router.get('/pricings',isAuthenticated, getAllPricings);
    router.post('/pricings/new',isAuthenticated, createNewPricing);
    router.delete('/pricings/:id',isAuthenticated, isOwner, deletePricing);
    router.get('/pricings/:id', isAuthenticated, isOwner, getSinglePricing);  

}

export default Pricings;