import { getAllProducts, deleteProduct, updatedProduct, createNewProduct } from '../controllers/products.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';

const Products = (router) => {
    router.get('/products',isAuthenticated, getAllProducts);
    router.post('/products/new',isAuthenticated, createNewProduct);
    router.delete('/products/:id',isAuthenticated, isOwner, deleteProduct);
    router.patch('/products/:id', isAuthenticated, isOwner, updatedProduct);  

}

export default Products;