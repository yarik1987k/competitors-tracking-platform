import { Login, Register } from '../controllers/authentication.js';
const Authentication = (router) => {
     router.post('/auth/register', Register)
     router.post('/auth/login', Login)
}
export default Authentication