import { getAllProjects, deleteProject, updatedProject, createNewProject } from '../controllers/projects.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';

const Projects = (router) => {
    router.get('/projects',isAuthenticated, getAllProjects);
    router.post('/projects/new',isAuthenticated, createNewProject);
    router.delete('/projects/:id',isAuthenticated, isOwner, deleteProject);
    router.patch('/projects/:id', isAuthenticated, isOwner, updatedProject);  

}

export default Projects;