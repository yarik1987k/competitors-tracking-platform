import { getAllUsers, deleteUser, updatedUser } from '../controllers/users.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';

const Users = (router) => {
    router.get('/users',isAuthenticated, getAllUsers);
    router.delete('/users/:id',isAuthenticated, isOwner, deleteUser);
    router.patch('/users/:id', isAuthenticated, isOwner, updatedUser);   
}

export default Users;