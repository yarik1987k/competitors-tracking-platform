export const extractPriceAndCurrency = (priceString) =>{
    const regex = /([0-9,.]+)\s*([^0-9,.]+)/;
    const match = priceString.match(regex);

    if (match && match.length === 3) {
        const [, price, currency] = match;
        const formattedPrice = price.replace(',', '.');
        return [parseFloat(formattedPrice), currency.trim()];
    } else {
        console.error('Failed to extract price and currency from:', priceString);
        return null;
    }
}