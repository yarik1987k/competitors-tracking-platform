import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import cors from 'cors';
import mongoose from 'mongoose';
import Router from './router/index.js';
import 'dotenv/config'; 

const App = express();
App.use(cors({
    credentials: true
}));
App.use(compression());
App.use(cookieParser());
App.use(bodyParser.json()); 

const Server = http.createServer(App);

Server.listen(process.env.BACK_PORT, () => {
    console.log('server is running on port http://localhost:'+process.env.BACK_PORT+'/');
});
const DB_URL = "mongodb+srv://" + process.env.DB_USER+ ":"+process.env.DB_PASS+"@pricing.fugaejg.mongodb.net/?retryWrites=true&w=majority&appName=pricing";
mongoose.Promise = Promise;
mongoose.connect(DB_URL);

const db = mongoose.connection;
db.on('open', () => console.log('Connected to the Database'));
db.on('error', (error) => console.log(error));
 
App.use('/', Router);