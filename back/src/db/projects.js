import mongoose from "mongoose";
const ProjectsSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: true},
    userId: {type: String, required: true},
    createdAt: { type: Date, default: Date.now }
});

export const ProjectModel = mongoose.model('Project', ProjectsSchema);
export const getProjects = () => ProjectModel.find();
export const getProjectByUserId = (userId) => ProjectModel.findOne({ userId });
export const getProjectById = (id) => ProjectModel.findById(id);
export const createProject = async (values) => {
    values.createdAt = new Date();
    const project = await new ProjectModel(values).save();
    return project.toObject();
};
export const deleteProjectById = (id) => ProjectModel.findByIdAndDelete({_id : id});
export const updateProjectById = (id, values) => ProjectModel.findByIdAndUpdate(id, values);
