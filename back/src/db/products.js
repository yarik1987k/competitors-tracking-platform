import mongoose from "mongoose";
const ProductsSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: true},
    startPrice: {type: String, required: true},
    projectId: {type: String, required: true},
    currency: {type: String, required: true},
    createdAt: { type: Date, default: Date.now }
});

export const ProductModel = mongoose.model('Product', ProductsSchema);
export const getProducts = () => ProductModel.find();
export const getProductsByProjectId = (projectID) => ProductModel.findOne({ projectID });
export const getProductById = (id) => ProductModel.findById(id);
export const createProduct = async (values) => {
    values.createdAt = new Date();
    const product = await new ProductModel(values).save();
    return product.toObject();
};
export const deleteProductById = (id) => ProductModel.findByIdAndDelete({_id : id});
export const updateProductById = (id, values) => ProductModel.findByIdAndUpdate(id, values);
