import mongoose from "mongoose";
const PricingSchema = new mongoose.Schema({
    name: {type: String, required: true},
    competitorId: {type: String, required: true},
    price: {type: String, required: true},
    createdAt: { type: Date, default: Date.now },
    createdTime: { type: String, default: getCurrentTime }
});
function getCurrentTime() {
    const now = new Date();
    const hours = now.getHours().toString().padStart(2, '0');
    const minutes = now.getMinutes().toString().padStart(2, '0');
    const seconds = now.getSeconds().toString().padStart(2, '0');
    return `${hours}:${minutes}:${seconds}`;
}
export const PricingModel = mongoose.model('Price', PricingSchema);
export const getPricings = () => PricingModel.find();
export const getPricingByCompetitorID = (productId) => PricingModel.findOne({ productId });
export const getPricingById = (id) => PricingModel.findById(id);
export const createPricing = async (values) => {
    values.createdAt = new Date();
    values.createdTime = getCurrentTime();
    const pricing = await new PricingModel(values).save();
    return pricing.toObject();
};
export const deletePricingById = (id) => PricingModel.findByIdAndDelete({_id : id});
export const updatePricingById = (id, values) => PricingModel.findByIdAndUpdate(id, values);
