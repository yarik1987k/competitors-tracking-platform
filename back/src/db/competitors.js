import mongoose from "mongoose";
const CompetitorsSchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: true},
    url: {type: String, required: true},
    selector: {type: String, required: true},
    selectorType: {type: String, required: true},
    startPrice: {type: String, required: true},
    productId: {type: String, required: true},
    currency: {type: String, required: true},
    createdAt: { type: Date, default: Date.now }
});

export const CompetitorModel = mongoose.model('Competitor', CompetitorsSchema);
export const getCompetitors = () => CompetitorModel.find();
export const getCompetitorsByProductID = (productId) => CompetitorModel.findOne({ productId });
export const getCompetitorById = (id) => CompetitorModel.findById(id);
export const createCompetitor = async (values) => {
    values.createdAt = new Date();
    const competitor = await new CompetitorModel(values).save();
    return competitor.toObject();
};
export const deleteCompetitorById = (id) => CompetitorModel.findByIdAndDelete({_id : id});
export const updateCompetitorById = (id, values) => CompetitorModel.findByIdAndUpdate(id, values);
